const actualPop = window.pop;
window.pop = function () {
    push();

    strokeWeight(3);
    stroke("black");
    line(-10,0,10,0);
    line(0,-10,0,10);

    strokeWeight(1);
    stroke("red");
    line(-10,0,10,0); 
    stroke("lime");
    line(0,-10,0,10);

    actualPop(); /* belongs to *our* push */
    actualPop(); /* belongs to *the outer* push */
};
