// slower
void(document.querySelector('video,audio').playbackRate-=0.25);

// faster
void(document.querySelector('video,audio').playbackRate+=0.25);
