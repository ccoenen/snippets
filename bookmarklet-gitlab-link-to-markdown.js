/*
	put this into a bookmarklet to copy the currently active link in
	markdown format. Useful if you wish to easily comment on a bunch of files
	but you also want the links to be human-readable.

	example url:
		https://example.com/orgname/repo-name/-/blob/abcdb0a8533057b753c7108b36d41b6c93ee0449/.gitignore#L2

	example output:
		[.gitinore Zeile 2](https://example.com/orgname/repo-name/-/blob/abcdb0a8533057b753c7108b36d41b6c93ee0449/.gitignore#L2)
		```
		(stuff from line 2)
		```
*/

(function () {
	const PATTERN = /^(?<full>.+\/blob\/(?<commit>[^\/]+)(\/.+)?\/(?<filename>(?<basename>[^\/#]+?)(?:\.(?<extension>[^.\/#]+))?)(?:#L(?<lines>[\d-]+))?)$/;

	const url = window.location.toString();
	const matches = url.match(PATTERN);
	if (!matches) {
		alert(`url not compatible?\n${url}`);
		return;
	}

	let lines = '';
	let content = '';
	let codeType = matches.groups.extension || 'txt';
	if (codeType === 'pde') { codeType = 'java'; } // Processing specialty
	if (matches.groups.lines) {
		lines = ` line ${matches.groups.lines}`;
		let [lower, upper] = matches.groups.lines.split('-');
		lower = parseInt(lower, 10);
		upper = parseInt(upper, 10) || lower;
		for (let i = lower; i <= upper; i++) {
			console.log(i);
			const line = document.querySelector(`#LC${i}`).innerText;
			content = content + line + '\n';
		}
		content = `\n\`\`\`${codeType}=${lower}\n${content}\`\`\`\n`;
	}

	let link = `[${decodeURI(matches.groups.filename)}${lines}](${matches.groups.full})`;
	if (content !== '') {
		link = link + ':' + content;
	}

	console.log(link);
	navigator.clipboard.writeText(link);

	if (matches.groups.commit.length < 40) {
		alert('beware: not a stable link to single commit. Press "y" first!');
	}
}());
